package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        // check input
        if (x == null || y == null) throw new IllegalArgumentException();

        if (x.size() == 0) return true;
        if (x.size() > y.size() || y.size() == 0) return false;

        Iterator yIter = y.iterator();
        for (Object xElement : x) {
            if (yIter.hasNext()) {
                while (yIter.hasNext()) { // go to the next equal element
                    if (xElement.equals(yIter.next()))
                        break;
                }
            } else { // there is no elements in y while x isn't ended
                return false;
            }
        }

        return true;
    }
}
