package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class Calculator {

    public static final int ROUND_DIGITS_NUMBER = 4;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        Parser myParser = new Parser();
        String resultingString = null;

        try {

            double result = myParser.evaluate(statement);

            if (result % 1 > 0){
                resultingString = Double.toString(round(result, ROUND_DIGITS_NUMBER));
            }else {
                resultingString = Integer.toString((int)result);
            }
        } catch (ParserException e) {
            resultingString = null;

        } catch (Exception e) {
            resultingString = null;
        }

        return resultingString;
    }

    /**
     * Round double with expected accuracy
     * @param value value to round
     * @param places digits number after rounding
     * @return
     */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
