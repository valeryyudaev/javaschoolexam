package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        // check the input
        if (inputNumbers.contains(null) || inputNumbers.size() > 100) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        // total number of nun-zero elements in the pyramide is the sum of an arithmetic progression
        int pyramidHeight = (int) (Math.sqrt((double) (1 + 8 * inputNumbers.size())) - 1) / 2;

        // build the pyramid
        int currentElement = 0;
        int width = pyramidHeight * 2 - 1;
        int[][] pyramid = new int[pyramidHeight][width];

        for (int i = 0; i < pyramidHeight; i++) {
            int positionInRow = pyramidHeight - i - 1;

            // todo extract function
            for (int l = 0; l < i + 1; l++) {
                pyramid[i][positionInRow] = inputNumbers.get(currentElement);
                currentElement++;
                positionInRow += 2;
            }
        }
        return pyramid;

    }
}